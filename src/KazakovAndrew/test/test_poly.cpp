#include <gtest/gtest.h>
#include "Polinom.h"

//
// Creation
//

TEST(TPolinom, can_create_poly) {
  ASSERT_NO_THROW(TPolinom p);
}

TEST(TPolinom, can_create_poly_from_an_array) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};

  ASSERT_NO_THROW(TPolinom p(monoms, 3));
}

TEST(TPolinom, can_create_poly_from_existed_one) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
  TPolinom p1(monoms, 3);

  ASSERT_NO_THROW(TPolinom p2(p1));
}

//
// Equality
//

TEST(TPolinom, equal_polys_are_equal) {
  int monoms[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
  TPolinom p1(monoms, 3), p2(monoms, 3);

  ASSERT_EQ(true, p1 == p2);
}

TEST(TPolinom, copied_poly_is_equal_to_itself) {
  int monoms[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
  TPolinom p1(monoms, 3);
  TPolinom p2(p1);

  ASSERT_EQ(true, p1 == p2);
}

//
// Get length
//

TEST(TPolinom, length_of_new_poly_is_zero) {
  TPolinom p;

  ASSERT_EQ(p.getListLength(), 0);
}

TEST(TPolinom, length_of_poly_is_calculated_correctly) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}, {5, 988}};
  TPolinom p(monoms, 4);

  ASSERT_EQ(p.getListLength(), 4);
}

TEST(TPolinom, length_of_poly_created_from_existed_one_is_calculated_correctly) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
  TPolinom p1(monoms, 3);
  TPolinom p2(p1);

  ASSERT_EQ(p2.getListLength(), 3);
}

//
// Assignment
//

TEST(TPolinom, can_assign_poly_to_empty_poly) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
  TPolinom p1(monoms, 3), p2;

  ASSERT_NO_THROW(p2 = p1);
}

TEST(TPolinom, assignment_of_not_empty_poly_to_empty_one_changes_its_length) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
  TPolinom p1(monoms, 3), p2;
  p2 = p1;

  ASSERT_EQ(p2.getListLength(), 3);
}

TEST(TPolinom, can_assign_poly_to_not_empty_poly) {
  int monoms1[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
  int monoms2[][2] = { { 10, 33 },{ -7, 39 } };
  TPolinom p1(monoms1, 3), p2(monoms2, 2);
  p2 = p1;

  ASSERT_NO_THROW(p2 = p1);
}

TEST(TPolinom, assignment_of_not_empty_poly_to_not_empty_one_changes_its_length) {
  int monoms1[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
  int monoms2[][2] = { {10, 33}, {-7, 39} };
  TPolinom p1(monoms1, 3), p2(monoms2, 2);
  p2 = p1;

  ASSERT_EQ(p2.getListLength(), 3);
}

TEST(TPolinom, can_assign_poly_to_itself) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
  TPolinom p(monoms, 3);

  ASSERT_NO_THROW(p = p);
}

TEST(TPolinom, assignment_of_poly_to_itself_doesnt_change_length_of_poly) {
  int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
  TPolinom p(monoms, 3);
  p = p;

  ASSERT_EQ(p.getListLength(), 3);
}

//
// Addition
//

TEST(TPolinom, can_add_empty_polys) {
  TPolinom p1, p2;

  ASSERT_NO_THROW(p1 + p2);
}

TEST(TPolinom, addition_works_correctly){
  int monoms1[][2] = {{10, 33}, {-7, 39}, {7, 40}, {1, 860}};
  int monoms2[][2] = {{10, 33}, {-7, 39}, {-7, 40}, {4, 900}, {888, 999}};
  int monoms3[][2] = {{20, 33}, {-14, 39}, {1, 860}, {4, 900}, {888, 999}};
  TPolinom p1(monoms1, 4), p2(monoms2, 5), p3, p4(monoms3, 5);

  p3 = p1 + p2;

  EXPECT_EQ(true, p3 == p4);
}
