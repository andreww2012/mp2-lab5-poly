// Абстрактный класс для объектов-значений списка

#pragma once

class TDatValue {
public:
  virtual TDatValue* GetCopy(void) = 0;
  ~TDatValue(void) {};
};
