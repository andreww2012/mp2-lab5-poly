#pragma once

#include "Monom.h"
#include "HeadRing.h"

#include <iostream>

// Класс полиномов
class TPolinom : public THeadRing {
public:
  TPolinom(int monoms[][2] = nullptr, const int length = 0);
  TPolinom(TPolinom& poly);
  TPolinom& operator=(TPolinom& poly);

  TMonom* GetMonom(void) const;

  double Calc(const int x, const int y, const int z);

  TPolinom operator+(TPolinom& poly);
  bool operator==(TPolinom& poly) const;

  friend std::ostream& operator<<(std::ostream& os, TPolinom& poly);
};
