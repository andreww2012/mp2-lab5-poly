#pragma once

#include "DatValue.h"

// Класс объектов-значений (мономов)
class TMonom : public TDatValue {
public:
  TMonom(const int coeff = 1, const int index = 0);
  TMonom& operator=(const TMonom&);

  virtual TDatValue* GetCopy(void) override;

  int GetCoeff(void);
  int GetIndex(void);
  void SetCoeff(const int&);
  void SetIndex(const int&);

  bool operator==(const TMonom&) const;
  bool operator<(const TMonom&) const;

protected:
  int coeff_;
  int index_;

  friend class TPolinom;
};
