#pragma once

#include "DatList.h"

// Класс циклических списков с заголовком
class THeadRing : public TDatList {
public:
  THeadRing(void);
  ~THeadRing(void);

  virtual void InsBeforeFirst(TDatValue* = nullptr) override;
  virtual void DelFirstLink(void) override;

protected:
  // Специальное звено-заголовок. Маркируется логически-недопустимыми
  // значениями коэффициента и индекса монома. Нужно для представления
  // нулевого полинома.
  TDatLink* link_head_;
};
