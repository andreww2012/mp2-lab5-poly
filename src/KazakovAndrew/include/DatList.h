#pragma once

#include "DatLink.h"

// Класс линейных списков
class TDatList {
public:
  enum TLinkPos {
    LINKPOS_FIRST,
    LINKPOS_CURRENT,
    LINKPOS_LAST
  };

  TDatList(void);
  ~TDatList(void);

  TDatValue* GetDatValue(TLinkPos mode = TLinkPos::LINKPOS_CURRENT) const;

  int GetCurrPos(void) const;
  void SetCurrPos(const int);

  int GetListLength(void) const;
  virtual bool IsListEnded(void) const;
  virtual bool IsEmpty(void) const;

  virtual void Reset(void);  // установка текущей позиции на начало
  void GoNext(void);  // сдвиг вправо текущего звена

  virtual void InsBeforeFirst(TDatValue* = nullptr);
  virtual void InsAfterLast(TDatValue* = nullptr);
  virtual void InsBeforeCurrent(TDatValue* = nullptr);

  virtual void DelFirstLink(void);
  virtual void DelCurrentLink(void);

  virtual void DelList(void);

protected:
  TDatLink* link_first_;
  TDatLink* link_last_;
  TDatLink* link_curr_;
  TDatLink* link_prev_;

  // Значение указателя, означающего конец списка. По умолчанию nullptr
  TDatLink* linkStop_;

  int curr_pos_;
  int list_length_;

protected:
  // Создание звена
  TDatLink* GetLink(TDatValue* data = nullptr, TDatLink* link = nullptr) const;

  // Удаление звена
  void DelLink(TDatLink*) const;
};
