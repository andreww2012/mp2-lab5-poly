#pragma once

#include "DatValue.h"

// Базовый класс для звеньев
class TRootLink {
public:
  explicit TRootLink(TRootLink* nextLink = nullptr);

  TRootLink* GetNextLink(void) const;
  void SetNextLink(TRootLink*);

  // Вставить звено между текущим и тем, на которое указывало текущее
  void InsNextLink(TRootLink*);

  virtual void SetDatValue(TDatValue* data) = 0;
  virtual TDatValue* GetDatValue(void) const = 0;

protected:
  TRootLink* next_link_;

  friend class TDatList;
};
