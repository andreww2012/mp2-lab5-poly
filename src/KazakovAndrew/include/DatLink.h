#pragma once

#include "RootLink.h"

// Звено с указателем на объект-значение (моном)
class TDatLink : public TRootLink {
public:
  TDatLink(TDatValue* data = nullptr, TRootLink* next_link = nullptr);

  TDatValue* GetDatValue(void) const;
  void SetDatValue(TDatValue* data);

  // Получить ссылку на след. звено, приведённую к типу TDatLink*
  TDatLink* GetNextDatLink(void) const;

protected:
  TDatValue* data_;

  friend class TDatList;
};
