#include "DatList.h"

TDatLink* TDatList::GetLink(TDatValue* data, TDatLink* link) const {
  return new TDatLink(data, link);
}

void TDatList::DelLink(TDatLink* link) {
  if (link != nullptr) {
    if (link->data_ != nullptr) {
        delete link->data_;
    }

    delete link;
  }
}

TDatList::TDatList() {
  link_first_ = link_last_ = link_curr_ = link_prev_ = link_stop_ = nullptr;
  list_length_ = 0;
  curr_pos_ = -1;
}

TDatList::~TDatList() {
  DelList();
}

TDatValue* TDatList::GetDatValue(TLinkPos mode) const {
  TDatLink* temp = nullptr;

  switch(mode) {
    case TLinkPos::LINKPOS_FIRST:
      temp = link_first_;
      break;

    case TLinkPos::LINKPOS_CURRENT:
      temp = link_curr_;
      break;

    case TLinkPos::LINKPOS_LAST:
      temp = link_last_;
      break;
  }

  return (temp == nullptr) ? nullptr : temp->GetDatValue();
}

int TDatList::GetCurrPos() const {
  return curr_pos_;
}

void TDatList::SetCurrPos(const int pos) {
  if (pos < 0 || pos > list_length_ - 1) {
    throw 2;
  }

  Reset();
  for (int i = 0; i < pos; GoNext());
}

int TDatList::GetListLength() const {
  return list_length_;
}

bool TDatList::IsListEnded() const {
  return link_curr_ == link_stop_;
}

bool TDatList::IsEmpty() const {
  return link_first_ == link_stop_;
}

void TDatList::Reset() {
  link_prev_ = nullptr;

  if (!IsEmpty()) {
    curr_pos_ = 0;
    link_curr_ = link_first_;
  } else {
    curr_pos_ = -1;
    link_curr_ = link_stop_;
  }
}

void TDatList::GoNext() {
  if (!IsListEnded()) {
    link_prev_ = link_curr_;
    link_curr_ = link_curr_->GetNextDatLink();
    curr_pos_++;
  }
}

void TDatList::InsBeforeFirst(TDatValue* data) {
  TDatLink* link = GetLink(data, link_first_);

  list_length_++;
  link_first_ = link;

  if (list_length_ == 1) {  // если вставляемое звено первое в списке
    Reset();
  } else if (curr_pos_ == 0) {  // список был непустой, но какая была позиция?
    link_curr_ = link;
  } else {
    curr_pos_++;
  }
}

void TDatList::InsAfterLast(TDatValue* data) {
  TDatLink* link = GetLink(data, link_stop_);

  list_length_++;

  // Список был непустой => "старый последний" эл-т должен указывать
  // на "новый последний"
  if (link_last_ != nullptr) {
    link_last_->SetNextLink(link);
  }

  // Меняем последний элемент
  link_last_ = link;

  // Если вставляемое звено первое в списке
  if (list_length_ == 1) {
    link_first_ = link;
    Reset();
  }

  // Если находились в конце списка, учитываем, что конец изменился
  if (IsListEnded()) {
    link_curr_ = link;
  }
}

void TDatList::InsBeforeCurrent(TDatValue* data) {
  if (IsEmpty() || link_curr_ == link_first_) {
    InsBeforeFirst(data);
    return;
  }

  // Далее мы уверены, что список непустой и мы находимся не в начале списка

  TDatLink* link = GetLink(data, link_curr_);
  list_length_++;

  link_prev_->SetNextLink(link);
  link_curr_ = link;
}

void TDatList::DelFirstLink() {
  if (IsEmpty())
    return;

  TDatLink* temp = link_first_;
  link_first_ = link_first_->GetNextDatLink();
  DelLink(temp);
  list_length_--;

  // Если удалили единственный элемент
  if (IsEmpty()) {
    link_last_ = link_stop_;
    Reset();
  } else {
    if (curr_pos_ == 0) {
      link_curr_ = link_first_;
    }

    if (curr_pos_ == 1) {
      link_prev_ = link_stop_;
    }

    if (curr_pos_ > 0) curr_pos_--;
  }
}

void TDatList::DelCurrentLink() {
  if (IsEmpty())
    return;

  // Далее мы уверены, что список непустой
  if (link_curr_ == link_first_) {
    DelFirstLink();
  } else {
    TDatLink* temp = link_curr_;
    if (link_curr_ == link_last_) {
      link_last_ = link_prev_;
      link_curr_ = link_stop_;
    } else {
      link_curr_ = link_curr_->GetNextDatLink();
    }
    // Предыдущий теперь указ. на новый текущий
    link_prev_->SetNextLink(link_curr_);
    DelLink(temp);
    list_length_--;
  }
}

void TDatList::DelList() {
  while (!IsEmpty()) {
    DelFirstLink();
  }

  curr_pos_ = -1;
  link_first_ = link_last_ = link_curr_ = link_prev_ = link_stop_;
}
