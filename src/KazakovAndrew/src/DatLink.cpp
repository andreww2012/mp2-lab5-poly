#include "DatLink.h"

TDatLink::TDatLink(TDatValue* data, TRootLink* nextLink) : data_(data),
TRootLink(nextLink) {}

TDatValue* TDatLink::GetDatValue() const {
  return data_;
}

void TDatLink::SetDatValue(TDatValue* data) {
  data_ = data;
}

TDatLink* TDatLink::getNextDatLink() const {
  return (TDatLink*)next_link_;
}
