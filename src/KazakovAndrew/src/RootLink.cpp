#include "RootLink.h"

TRootLink::TRootLink(TRootLink* nextLink) : nextLink_(nextLink) {}

TRootLink* TRootLink::GetNextLink() const {
  return next_link_;
}

void TRootLink::SetNextLink(TRootLink* next_link) {
  next_link_ = next_link;
}

void TRootLink::InsNextLink(TRootLink* link) {
  TRootLink* temp = next_link_;
  next_link_ = link;

  if (link != nullptr) {
      link->next_link_ = temp;
  }
}
