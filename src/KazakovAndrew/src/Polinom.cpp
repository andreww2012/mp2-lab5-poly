#include "Polinom.h"

#include <iostream>

TPolinom::TPolinom(int monoms[][2], const int length) {
  // Определяем, как выглядит фиктивное звено-заголовок
  link_head_->SetDatValue(new TMonom(0, -1));

  // Создаём остальные звенья
  for (int i = 0; i < length; i++) {
    InsAfterLast(new TMonom(monoms[i][0], monoms[i][1]));
  }
}

TPolinom::TPolinom(TPolinom& poly) {
  link_head_->SetDatValue(new TMonom(0, -1));

  for (poly.Reset(); !poly.IsListEnded(); poly.GoNext()) {
      InsAfterLast(poly.GetMonom()->GetCopy());
  }

  Reset();
}

double TPolinom::Calc(const int x, const int y, const int z) {
  TMonom* monom;
  double result = 0;

  // Степени монома
  int a, b, c;

  for (Reset(); !IsListEnded(); GoNext()) {
    monom = GetMonom();
    a = monom->GetIndex() / 100;
    b = (monom->GetIndex() % 100) / 10;
    c = monom->GetIndex() % 10;
    result += monom->GetCoeff() * pow(x, a) * pow(y, b) * pow(z, c);
  }

  return result;
}

TMonom* TPolinom::GetMonom() const {
  return (TMonom*)GetDatValue();
}

// Чтобы сложение полиномов работало корректно, предполагается, что
// все полиномы отсортированы по возрастанию степеней мономов
TPolinom TPolinom::operator+(TPolinom& poly) {
  TPolinom temp(*this);

  if (!temp.GetListLength())
    return temp = poly;

  if (!poly.GetListLength())
    return temp;

  TMonom *temp_curr_monom, *copied_curr_monom;
  temp.GoNext();
  poly.Reset();
  poly.GoNext();

  while (true) {
    temp_curr_monom = temp.GetMonom();
    copied_curr_monom = poly.GetMonom();

    // !!! Все полиномы отсортированы по ВОЗРАСТАНИЮ степеней мономов
    // Индекс текущего монома больше
    if (temp_curr_monom->index_ > copied_curr_monom->index_) {
      temp.InsBeforeCurrent(new TMonom(copied_curr_monom->coeff_, copied_curr_monom->index_));
      poly.GoNext();
    // Индекс текущего монома меньше
    } else if (temp_curr_monom->index_ < copied_curr_monom->index_) {
      temp.GoNext();
    // Индексы совпадают
    } else {
      // Условие конца цикла: прошлись по обоим полиномам
      if (temp_curr_monom->index_ == -1)
        break;

      temp_curr_monom->coeff_ += copied_curr_monom->coeff_;

      // Ситуация, когда при сложении получился нулевой коэффициент
      if (temp_curr_monom->coeff_ == 0) {
        temp.DelCurrentLink();
      } else {
        temp.GoNext();
      }

      poly.GoNext();
    }
  }  // while

  return temp;
}

TPolinom& TPolinom::operator=(TPolinom& poly) {
  if (&poly != this) {
    DelList();

    link_head_->SetDatValue(new TMonom(0, -1));

    for (poly.Reset(); !poly.IsListEnded(); poly.GoNext()) {
      InsAfterLast(poly.GetMonom()->GetCopy());
    }
  }

  return *this;
}

bool TPolinom::operator==(TPolinom& poly) const {
  if (GetListLength() != poly.GetListLength())
    return false;

  for (Reset(), poly.Reset(); !IsListEnded(); GoNext(), poly.GoNext()) {
    if (!(*GetMonom() == *poly.GetMonom()))
      return false;
  }

  return true;
}

std::ostream& operator<<(std::ostream& os, TPolinom& poly) {
  for (poly.Reset(); !poly.IsListEnded(); poly.GoNext())
    os << poly.GetMonom()->GetCoeff() << " " << poly.GetMonom()->GetIndex()
    << std::endl;

  return os;
}
