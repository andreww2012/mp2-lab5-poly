#include "Monom.h"

TMonom::TMonom(const int coeff, const int index) : coeff_(coeff),
index_(index) {}

TMonom& TMonom::operator=(const TMonom& monom) {
  coeff_ = monom.coeff_;
  index_ = monom.index_;

  return *this;
}

TDatValue* TMonom::GetCopy() {
  return new TMonom(coeff_, index_);
}

void TMonom::SetCoeff(const int coeff&) {
  coeff_ = coeff;
}

void TMonom::SetIndex(const int index&) {
  index_ = index;
}

int TMonom::GetCoeff() const {
  return coeff_;
}

int TMonom::GetIndex() const {
  return index_;
}

bool TMonom::operator==(const TMonom& monom) const {
  return (coeff_ == monom.coeff_) && (index_ == monom.index_);
}

bool TMonom::operator<(const TMonom& monom) const {
  return index_ < monom.index_;
}
