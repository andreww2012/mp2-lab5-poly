#include "HeadRing.h"

THeadRing::THeadRing() : TDatList() {
  // Первый элемент в списке - фиктивное звено-заголовок
  TDatList::InsBeforeFirst();
  link_head_ = link_first_;

  // Делаем список циклическим, убираем лишнюю длину списка
  link_stop_ = link_head_;
  Reset();
  list_length_ = 0;

  link_head_->SetNextLink(link_head_);
}

THeadRing::~THeadRing() {
  TDatList::DelList();
  DelLink(link_head_);
}

void THeadRing::InsBeforeFirst(TDatValue* data) {
  TDatList::InsBeforeFirst();
  link_head_->SetNextLink(link_first_);
}

void THeadRing::delFirstLink() {
  TDatList::DelFirstLink();
  link_head_->setNextLink(link_first_);
}
