# Отчёт по выполненной работе "Структура данных Полином"
## Введение

**Цель данной работы** — создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:
* ввод полинома;
* организация хранения полинома;
* удаление введенного ранее полинома;
* копирование полинома;
* сложение двух полиномов;
* вычисление значения полинома при заданных значениях переменных;
* вывод.

Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:
* поддержка понятия текущего звена;
* вставка звеньев в начало, после текущей позиции и в конец списков;
* удаление звеньев в начале и в текущей позиции списков;
* организация последовательного доступа к звеньям списка (итератор).

При выполнении лабораторной работы можно использовать следующие основные предположения:
1. Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
2. Степени переменных полиномов не могут превышать значения 9.
3. Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

Была предложена следующая иерарахия классов, реализующих мономы, звенья, линейные списки и полиномы:

![](classes.png)

> **Примечание**. Далее приведены тексты файлов не целиком, а только лишь описания и реализации классов.

## Классы для работы с мономами
В первую очередь предложено было реализовать абстрактный класс объектов-значений TDatValue. Он является основой для класса "Моном" (TMonom), и определяет операцию получения копии данного объекта. Сам по себе TDatValue мог бы пригодиться, например, для класса, представляющий моном по-другому.

### Класс TDatValue - абстрактный класс для объектов-значений списка
```C++
class TDatValue {
public:
    virtual TDatValue* getCopy() = 0;
    ~TDatValue() {};
};
```

Далее имеем класс с самим мономами. Добавляем конструктор, методы для установки и удаления коэффициента и индекса (степени монома). Упрощаем доступ к коэффициенту и индексу классу TPolinom.

### Класс TMonom - класс объектов-значений (мономов)
```C++
class TMonom : public TDatValue {
public:
    TMonom(int coeff = 1, int index = 0);
    virtual TDatValue* getCopy() override;

    void setCoeff(int coeff);
    int getCoeff(void);
    void setIndex(int index);
    int getIndex(void);

    TMonom& operator=(const TMonom& monom);
    bool operator==(const TMonom& monom);
    bool operator<(const TMonom& monom);

protected:
    int coeff_;
    int index_;

    friend class TPolinom;
};
```

```C++
TMonom::TMonom(int coeff, int index) : coeff_(coeff), index_(index) {
}

TDatValue* TMonom::getCopy() {
    return new TMonom(coeff_, index_);
}

void TMonom::setCoeff(int coeff) {
    coeff_ = coeff;
}

int TMonom::getCoeff() {
    return coeff_;
}

void TMonom::setIndex(int index) {
    index_ = index;
}

int TMonom::getIndex() {
    return index_;
}

TMonom& TMonom::operator=(const TMonom& monom) {
    coeff_ = monom.coeff_;
    index_ = monom.index_;

    return *this;
}

bool TMonom::operator==(const TMonom& monom) {
    return (coeff_ == monom.coeff_) && (index_ == monom.index_);
}

bool TMonom::operator<(const TMonom& monom) {
    return index_ < monom.index_;
}
```

## Классы для работы со звеньями

Звено - это нечто большее, чем моном. Базовый класс со звеньями (TRootLink) определяет ссылку на следующее звено, а его наследник, класс TDatLink, добавляет к звену данные, т. е. сам моном.

### Класс TRootLink - базовый класс для звеньев
```C++
class TRootLink {
public:
    TRootLink(TRootLink* nextLink = nullptr);

    TRootLink* getNextLink(void);
    void setNextLink(TRootLink* nextLink);

    // Вставить звено между текущим и тем, на которое указывало текущее
    void insNextLink(TRootLink* link);

    // Работа с данными, содержащимися в данном звене
    virtual void setDatValue(TDatValue* data) = 0;
    virtual TDatValue* getDatValue(void) = 0;

protected:
    TRootLink* nextLink_;

    friend class TDatList;
};
```

```C++
TRootLink::TRootLink(TRootLink* nextLink) : nextLink_(nextLink) {
}

TRootLink* TRootLink::getNextLink() {
    return nextLink_;
}

void TRootLink::setNextLink(TRootLink* nextLink) {
    nextLink_ = nextLink;
}

void TRootLink::insNextLink(TRootLink* link) {
    TRootLink* temp = nextLink_;
    nextLink_ = link;

    if (link != nullptr) {
        link->nextLink_ = temp;
    }
}
```

### Класс TDatLink - звено с указателем на объект-значение (моном)
```C++
class TDatLink : public TRootLink {
public:
    TDatLink(TDatValue* data = nullptr, TRootLink* nextLink = nullptr);

    TDatValue* getDatValue(void);
    void setDatValue(TDatValue* data);

    // Получить ссылку на след. звено, приведённую к типу TDatLink*
    TDatLink* getNextDatLink(void);

protected:
    TDatValue* data_;

    friend class TDatList;
};
```

```C++
TDatLink::TDatLink(TDatValue* data, TRootLink* nextLink) : data_(data), TRootLink(nextLink) {
}

TDatValue* TDatLink::getDatValue() {
    return data_;
}

void TDatLink::setDatValue(TDatValue* data) {
    data_ = data;
}

TDatLink* TDatLink::getNextDatLink() {
    return (TDatLink*)nextLink_;
}
```

## Классы для работы с полиномами

Полином представляет из себя линейный список. Базовый линейный список реализует класс TDatList, его наследник THeadRing вводит циклические списки и специальное фиктивное звено-заголовок для представления нулевых списков, а его наследник, класс TPolinom, реализует методы для работы с полиномами, а также определяет нужный вид звена-заголовка: им станет моном с недопустимым значением степени.

### Класс TDatList - класс линейных списков
```C++
// Режим выбора значения: из какого звена взять?
enum TLinkPos {
    LINKPOS_FIRST, LINKPOS_CURRENT, LINKPOS_LAST
};

class TDatList {
public:
    TDatList(void);
    ~TDatList(void);

    TDatValue* getDatValue(TLinkPos mode = LINKPOS_CURRENT) const;

    int getCurrPos(void) const;
    void setCurrPos(int pos);

    int getListLength(void) const;
    virtual bool isListEnded(void) const;
    virtual bool isEmpty(void) const;

    // Установка текущей позиции на начало
    virtual void reset(void);

    // Сдвиг вправо текущего звена (=false после применения для посл. звена списка)
    void goNext(void);

    virtual void insBeforeFirst(TDatValue* data = nullptr);
    virtual void insAfterLast(TDatValue* data = nullptr);
    virtual void insBeforeCurrent(TDatValue* data = nullptr);

    virtual void delFirstLink(void);
    virtual void delCurrentLink(void);

    // Удаление списка
    virtual void delList(void);

protected:
    TDatLink* linkFirst_;
    TDatLink* linkLast_;
    TDatLink* linkCurr_;
    TDatLink* linkPrev_;

    // Значение указателя, означающего конец списка. По умолчанию nullptr
    TDatLink* linkStop_;

    int currPos_;
    int listLength_;

protected:
    // Создание звена
    TDatLink* getLink(TDatValue* data = nullptr, TDatLink* link = nullptr);

    // Удаление звена
    void delLink(TDatLink* link);
};
```

```C++
// Protected methods

TDatLink* TDatList::getLink(TDatValue* data, TDatLink* link) {
    return new TDatLink(data, link);
}

void TDatList::delLink(TDatLink* link) {
    if (link != nullptr) {
        if (link->data_ != nullptr) {
            delete link->data_;
        }

        delete link;
    }
}

// Public methods

TDatList::TDatList() {
    linkFirst_ = linkLast_ = linkCurr_ = linkPrev_ = linkStop_ = nullptr;
    listLength_ = 0;
    currPos_ = -1;
}

TDatList::~TDatList() {
    delList();
}

TDatValue* TDatList::getDatValue(TLinkPos mode) const {
    TDatLink* temp = nullptr;

    switch(mode) {
        case LINKPOS_FIRST:
            temp = linkFirst_;
            break;

        case LINKPOS_CURRENT:
            temp = linkCurr_;
            break;

        case LINKPOS_LAST:
            temp = linkLast_;
            break;
    }

    return (temp == nullptr) ? nullptr : temp->getDatValue();
}

int TDatList::getCurrPos() const {
    return currPos_;
}

void TDatList::setCurrPos(int pos) {
    if (pos < 0 || pos > listLength_ - 1) {
        throw 2;
    }

    reset();
    for (int i = 0; i < pos; goNext());
}

int TDatList::getListLength() const {
    return listLength_;
}

bool TDatList::isListEnded() const {
    return linkCurr_ == linkStop_;
}

bool TDatList::isEmpty() const {
    return linkFirst_ == linkStop_;
}

void TDatList::reset() {
    linkPrev_ = nullptr;

    if (!isEmpty()) {
        currPos_ = 0;
        linkCurr_ = linkFirst_;
    } else {
        currPos_ = -1;
        linkCurr_ = linkStop_;
    }
}

void TDatList::goNext() {
    if (!isListEnded()) {
        linkPrev_ = linkCurr_;
        linkCurr_ = linkCurr_->getNextDatLink();
        currPos_++;
    }
}

void TDatList::insBeforeFirst(TDatValue* data) {
    TDatLink* link = getLink(data, linkFirst_);
    listLength_++;
    linkFirst_ = link;

    // Если вставляемое звено первое в списке
    if (listLength_ == 1) {
        reset();
    }
    // Список был непустой, но какая была позиция?
    else if (currPos_ == 0) {
        linkCurr_ = link;
    } else {
        currPos_++;
    }
}

void TDatList::insAfterLast(TDatValue* data) {
    TDatLink* link = getLink(data, linkStop_);
    listLength_++;

    // Список был непустой => "старый последний" эл-т должен указывать на "новый последний"
    if (linkLast_ != nullptr)
        linkLast_->setNextLink(link);

    // Меняем последний элемент
    linkLast_ = link;

    // Если вставляемое звено первое в списке
    if (listLength_ == 1) {
        linkFirst_ = link;
        reset();
    }

    // Если находились в конце списка, учитываем, что конец изменился
    if (isListEnded()) {
        linkCurr_ = link;
    }
}

void TDatList::insBeforeCurrent(TDatValue* data) {
    if (isEmpty() || linkCurr_ == linkFirst_) {
        insBeforeFirst(data);
        return;
    }

    // Далее мы уверены, что список непустой и мы находимся не в начале списка

    TDatLink* link = getLink(data, linkCurr_);
    listLength_++;

    linkPrev_->setNextLink(link);
    linkCurr_ = link;
}

void TDatList::delFirstLink() {
    if (isEmpty())
        return;

    TDatLink* temp = linkFirst_;
    linkFirst_ = linkFirst_->getNextDatLink();
    delLink(temp);
    listLength_--;

    // Если удалили единственный элемент
    if (isEmpty()) {
        linkLast_ = linkStop_;
        reset();
    } else {
        if (currPos_ == 0) {
            linkCurr_ = linkFirst_;
        }

        if (currPos_ == 1) {
            linkPrev_ = linkStop_;
        }

        if (currPos_ > 0) currPos_--;
    }
}

void TDatList::delCurrentLink() {
    if (isEmpty())
        return;

    // Далее мы уверены, что список непустой
    if (linkCurr_ == linkFirst_) {
        delFirstLink();
    } else {
        TDatLink* temp = linkCurr_;
        if (linkCurr_ == linkLast_) {
            linkLast_ = linkPrev_;
            linkCurr_ = linkStop_;
        } else {
            linkCurr_ = linkCurr_->getNextDatLink();
        }
        linkPrev_->setNextLink(linkCurr_); // предыдущий теперь указ. на новый текущий
        delLink(temp);
        listLength_--;
    }
}

void TDatList::delList() {
    while (!isEmpty()) {
        delFirstLink();
    }

    currPos_ = -1;
    linkFirst_ = linkLast_ = linkCurr_ = linkPrev_ = linkStop_;
}
```

### Класс THeadRing - класс циклических списков с заголовком
```C++
class THeadRing : public TDatList {
public:
    THeadRing();
    ~THeadRing();

    virtual void insBeforeFirst(TDatValue* data = nullptr) override;
    virtual void delFirstLink(void) override;

protected:
    // Специальное звено-заголовок. Маркируется логически-недопустимыми значениями коэффициента
    // и индекса монома. Нужно для представления нулевого полинома.
    TDatLink* linkHead_;
};
```

```C++
THeadRing::THeadRing() : TDatList() {
    // Первый элемент в списке - фиктивное звено-заголовок
    TDatList::insBeforeFirst();
    linkHead_ = linkFirst_;

    // Делаем список циклическим, убираем лишнюю длину списка
    linkStop_ = linkHead_;
    reset();
    listLength_ = 0;

    linkHead_->setNextLink(linkHead_);
}

THeadRing::~THeadRing() {
    TDatList::delList();
    delLink(linkHead_);
}

void THeadRing::insBeforeFirst(TDatValue* data) {
    TDatList::insBeforeFirst();
    linkHead_->setNextLink(linkFirst_);
}

void THeadRing::delFirstLink() {
    TDatList::delFirstLink();
    linkHead_->setNextLink(linkFirst_);
}
```

### Класс TPolinom - класс полиномов
```C++
class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = nullptr, int length = 0);
    TPolinom(TPolinom& poly);

    TMonom* getMonom() const;

    double calc(int x, int y, int z);

    TPolinom operator+(TPolinom& poly);
    TPolinom& operator=(TPolinom& poly);
    bool operator==(TPolinom& poly);

    friend std::ostream& operator<<(std::ostream& os, TPolinom& poly);
};
```

```C++
TPolinom::TPolinom(int monoms[][2], int length) {
    // Определяем, как выглядит фиктивное звено-заголовок
    linkHead_->setDatValue(new TMonom(0, -1));

    // Создаём остальные звенья
    for (int i = 0; i < length; i++) {
        insAfterLast(new TMonom(monoms[i][0], monoms[i][1]));
    }
}

TPolinom::TPolinom(TPolinom& poly) {
    linkHead_->setDatValue(new TMonom(0, -1));

    for (poly.reset(); !poly.isListEnded(); poly.goNext()) {
        insAfterLast(poly.getMonom()->getCopy());
    }

    reset();
}

double TPolinom::calc(int x, int y, int z) {
    TMonom* monom;
    double result = 0;
    // Степени монома
    int a, b, c;

    for (reset(); !isListEnded(); goNext()) {
        monom = getMonom();
        a = monom->getIndex() / 100;
        b = (monom->getIndex() % 100) / 10;
        c = monom->getIndex() % 10;
        result += monom->getCoeff() * pow(x, a) * pow(y, b) * pow(z, c);
    }

    return result;
}

TMonom* TPolinom::getMonom() const {
    return (TMonom*)getDatValue();
}

// Чтобы сложение полиномов работало корректно, предполагается, что
// все полиномы отсортированы по возрастанию степеней мономов
TPolinom TPolinom::operator+(TPolinom& poly) {
    TPolinom temp(*this);

    if (!temp.getListLength()) return temp = poly;
    if (!poly.getListLength()) return temp;

    TMonom *tempCurrMonon, *copiedCurrMonom;
    temp.goNext();
    poly.reset();
    poly.goNext();

    while (true) {
        tempCurrMonon = temp.getMonom();
        copiedCurrMonom = poly.getMonom();

        // !!! Все полиномы отсортированы по ВОЗРАСТАНИЮ степеней мономов
        // Индекс текущего монома больше
        if (tempCurrMonon->index_ > copiedCurrMonom->index_) {
            temp.insBeforeCurrent(new TMonom(copiedCurrMonom->coeff_, copiedCurrMonom->index_));
            poly.goNext();
        // Индекс текущего монома меньше
        } else if (tempCurrMonon->index_ < copiedCurrMonom->index_) {
            temp.goNext();
        // Индексы совпадают
        } else {
            // Условие конца цикла: прошлись по обоим полиномам
            if (tempCurrMonon->index_ == -1)
                break;

            tempCurrMonon->coeff_ += copiedCurrMonom->coeff_;

            // Ситуация, когда при сложении получился нулевой коэффициент
            if (tempCurrMonon->coeff_ == 0) {
                temp.delCurrentLink();
            } else {
                temp.goNext();
            }

            poly.goNext();
        }
    }

    return temp;
}

TPolinom& TPolinom::operator=(TPolinom& poly) {
    if (&poly != this) {
        delList();

        linkHead_->setDatValue(new TMonom(0, -1));

        for (poly.reset(); !poly.isListEnded(); poly.goNext()) {
            insAfterLast(poly.getMonom()->getCopy());
        }
    }

    return *this;
}

bool TPolinom::operator==(TPolinom& poly) {
    if (getListLength() != poly.getListLength())
        return false;

    for (reset(), poly.reset(); !isListEnded(); goNext(), poly.goNext()) {
        if (!(*getMonom() == *poly.getMonom()))
            return false;
    }

    return true;
}

std::ostream& operator<<(std::ostream& os, TPolinom& poly) {
    for (poly.reset(); !poly.isListEnded(); poly.goNext())
        os << poly.getMonom()->getCoeff() << " " << poly.getMonom()->getIndex() << std::endl;

    return os;
}
```

### Тесты Google Test для класса TPolinom
```C++
//
// Creation
//

TEST(TPolinom, can_create_poly) {
    ASSERT_NO_THROW(TPolinom p);
}

TEST(TPolinom, can_create_poly_from_an_array) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};

    ASSERT_NO_THROW(TPolinom p(monoms, 3));
}

TEST(TPolinom, can_create_poly_from_existed_one) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
    TPolinom p1(monoms, 3);

    ASSERT_NO_THROW(TPolinom p2(p1));
}

//
// Equality
//

TEST(TPolinom, equal_polys_are_equal) {
    int monoms[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
    TPolinom p1(monoms, 3), p2(monoms, 3);

    ASSERT_EQ(true, p1 == p2);
}

TEST(TPolinom, copied_poly_is_equal_to_itself) {
    int monoms[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
    TPolinom p1(monoms, 3);
    TPolinom p2(p1);

    ASSERT_EQ(true, p1 == p2);
}

//
// Get length
//

TEST(TPolinom, length_of_new_poly_is_zero) {
    TPolinom p;

    ASSERT_EQ(p.getListLength(), 0);
}

TEST(TPolinom, length_of_poly_is_calculated_correctly) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}, {5, 988}};
    TPolinom p(monoms, 4);

    ASSERT_EQ(p.getListLength(), 4);
}

TEST(TPolinom, length_of_poly_created_from_existed_one_is_calculated_correctly) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
    TPolinom p1(monoms, 3);
    TPolinom p2(p1);

    ASSERT_EQ(p2.getListLength(), 3);
}

//
// Assignment
//

TEST(TPolinom, can_assign_poly_to_empty_poly) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
    TPolinom p1(monoms, 3), p2;

    ASSERT_NO_THROW(p2 = p1);
}

TEST(TPolinom, assignment_of_not_empty_poly_to_empty_one_changes_its_length) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
    TPolinom p1(monoms, 3), p2;
    p2 = p1;

    ASSERT_EQ(p2.getListLength(), 3);
}

TEST(TPolinom, can_assign_poly_to_not_empty_poly) {
    int monoms1[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
    int monoms2[][2] = { { 10, 33 },{ -7, 39 } };
    TPolinom p1(monoms1, 3), p2(monoms2, 2);
    p2 = p1;

    ASSERT_NO_THROW(p2 = p1);
}

TEST(TPolinom, assignment_of_not_empty_poly_to_not_empty_one_changes_its_length) {
    int monoms1[][2] = { { 10, 33 },{ -7, 39 },{ 1, 860 } };
    int monoms2[][2] = { {10, 33}, {-7, 39} };
    TPolinom p1(monoms1, 3), p2(monoms2, 2);
    p2 = p1;

    ASSERT_EQ(p2.getListLength(), 3);
}

TEST(TPolinom, can_assign_poly_to_itself) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
    TPolinom p(monoms, 3);

    ASSERT_NO_THROW(p = p);
}

TEST(TPolinom, assignment_of_poly_to_itself_doesnt_change_length_of_poly) {
    int monoms[][2] = {{10, 33}, {-7, 39}, {1, 860}};
    TPolinom p(monoms, 3);
    p = p;

    ASSERT_EQ(p.getListLength(), 3);
}

//
// Addition
//

TEST(TPolinom, can_add_empty_polys) {
    TPolinom p1, p2;

    ASSERT_NO_THROW(p1 + p2);
}

TEST(TPolinom, addition_works_correctly){
    int monoms1[][2] = {{10, 33}, {-7, 39}, {7, 40}, {1, 860}};
    int monoms2[][2] = {{10, 33}, {-7, 39}, {-7, 40}, {4, 900}, {888, 999}};
    int monoms3[][2] = {{20, 33}, {-14, 39}, {1, 860}, {4, 900}, {888, 999}};
    TPolinom p1(monoms1, 4), p2(monoms2, 5), p3, p4(monoms3, 5);

    p3 = p1 + p2;

    EXPECT_EQ(true, p3 == p4);
}
```

![](tests.png)

## Выводы

Была спроектирована система классов, определяющая такие сущности, как "моном", "звено" и "линейный список". Прелесть построенной иерархии в том, что число многие классы можно использовать по отдельности в совершенно других проектах и задачах: мономы, звенья, отдельно линейные списки, циклические списки и полиномы. Данный учебный проект - хорошая демонстрация некоторых правильных принципов проектирования ПО: мы значительно абстрагировали классы друг от друга, таким образом дав возможность использовать их по отдельности, а значит, скорее всего, сократили время на дублирование кода в новых проектах, в которых предполагается использование созданных нами сущностей.
